// index.ios.js

import { ARKit, withProjectedPosition } from 'react-native-arkit';
import { AppRegistry, Dimensions, View } from 'react-native';
import React, { Component } from 'react';

const diffuse = 'white';

const { width: windowWidth, height: windowHeight } = Dimensions.get('window');

export default class ReactNativeARKit extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ARKit
          style={{ flex: 1 }}
          debug
          planeDetection
          lightEstimationEnabled
          onPlaneDetected={console.log} // event listener for plane detection
          onPlaneUpdate={console.log} // event listener for plane update
        >
          
          <ARKit.Model
            id="test_dae"
            position={{ x: -0.2, y: 0, z: 0 }}
            model={ { file: 'art.scnassets/aj/aj.dae', scale: 0.001 }}
          />

        </ARKit>
      </View>
    );
  }
}
